# Link to GitLab repository
https://gitlab.com/simone989/2020_assignment1_pipilinepower

# Pipeline Power

Pipeline Power is a demo project created for the Software Development Process course delivered at University of Milano - Bicocca. The aim of the project is to provide an overview about CI/CD pipelines and involved tools.
To support the pipeline we created a web app with React. The goal of the application is to display on the home page the current version of the app fetched from the Git Lab repository of the project with some more information about the latest commit.

# Pipeline breackdown
The different stages of the pipeline are:
  - build
  - verify
  - unitTest
  - integrationTest
  - package
  - release
  - deploy

### 1. build
In this stage we call ```yarn``` to install al the dependencies
### 2. verify
This stage is  runned by ```yarn verify``` which is making static code analysis with Eslint
### 3. unitTest
Is runned with ```yarn unit-test```
### 4. integrationTest
Is runned with ```yarn integration-test```.
### 5. package
We create an artifacts that we are going to use later for the Deploy stage, to allow us to deploy the project to firebase hosting. The script is runned by  ```yarn build``` and the path to where the artefacts is located is   ```public/```
### 6. release
This stage pushes the repository, a new release tag version and create a new release inside Gitlab repository. The release tag is the one displayed in the home page of the web app created to support the pipeline.
In this stage we used three Environment variables which are:
 - GITLAB_USER_EMAIL
 - GITLAB_USER_NAME
 - GITLAB_ACCESS_TOKEN
 
This stage is only triggered when the event who triggered the pipeline is in the master branch

### 7. deploy
It's the final stage of the pipeline where we deploy the artifacts, created inside our package stage, to firebase. In this stage we used another Environment variable which is:
 - FIREBASE_DEPLOY_TOKEN
 
As with the release stage also this stage is only triggered if the branch is set to master

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

```
node version v10.19.0
npm install -g yarn
```


### Run local

```
yarn start
```

### Deploy

```
firebase login
firebase use [PROJECT NAME]
yarn build
yarn deploy
```

## Running the tests

### Unit test
```
yarn unit-test
```
 
### Intagration test
```
yarn integration-test
```

### Break down into end to end tests

For __Unit__ testing we have a test suit made of two tests: 
1. One checking if our component RepositoryForm is render correctly 
2. One checking if handleSubmit function is called when button is pressed
 
For __Integration__ testing we check if the version of the repository fetched calling gitlab API matches the one contained in the DOM


## Built With

* [React Js](https://reactjs.org/) - The web framework used
* [Typescript](https://www.typescriptlang.org/) Program language used
* [Firebase](https://firebase.google.com/docs/hosting) - Hosting service

## Authors

* **Simone Di Mauro** - [Linkedin](https://www.linkedin.com/in/simone-di-mauro-2782a0bb) [Github](https://github.com/simone989) [Telegram](https://t.me/simone989)
* **Alessandro Maggi** - [Linkedin](https://www.linkedin.com/in/alessandro-maggi-a06b99135/) [Github](https://github.com/alemaggi)

## License

This project is licensed under the Gnu License - see the [LICENSE.md](LICENSE.md) file for details

