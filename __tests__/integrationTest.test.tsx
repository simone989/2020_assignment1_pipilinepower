/**
 * INTEGRATION TEST
 */
import { RepositoryForm } from './../src/app/components/DomainForm/index'
import { getLastReleaseTagFromRepository } from './../src/app/api/index'

import * as React from 'react';
import * as Enzyme from 'enzyme';
// tslint:disable-next-line: no-implicit-dependencies
import * as Adapter from 'enzyme-adapter-react-16';
import ReactJson from 'react-json-view';
// tslint:disable-next-line: no-implicit-dependencies
import * as MutationObserver from '@sheerun/mutationobserver-shim'
import { Home } from './../src/app/pages/Home/index'
// tslint:disable-next-line: no-implicit-dependencies
import { waitFor } from "@testing-library/dom";

window.MutationObserver = MutationObserver;
const routeComponentPropsMock = {
    // add jest.fn() as needed to any of the objects
    history: {} as any,
    location: {} as any,
    match: {} as any,
  }

Enzyme.configure({ adapter: new Adapter() });

it("Check if version from API matches the DOM", async () => {
    //API Call
    const commitObject = await getLastReleaseTagFromRepository();



    const homeWrapper = Enzyme.shallow(<Home {...routeComponentPropsMock} />);
    //Expect to find the repositoryForm
    const repositoryForms = homeWrapper.find(RepositoryForm);
    expect(repositoryForms.length).toBe(1);
    const repositoryForm = homeWrapper.find(RepositoryForm).at(0);

    //call on submit
    repositoryForm.props().onSubmit();

    await waitFor(() => {
        //Printing value to check results
        console.log("ReactJson: " + homeWrapper.find(ReactJson).length);
        console.log("Tag: " + homeWrapper.find('#tag').text());
    })
    console.log("Alla fine il tag è: " + homeWrapper.find('#tag').text());
    expect(homeWrapper.find('#tag').text()).toBe((commitObject as any).name);
})
