import { RepositoryForm } from './../src/app/components/DomainForm/index'
/**
 * UNIT TEST
 */
import * as React from 'react';
import * as Enzyme from 'enzyme';
// tslint:disable-next-line: no-implicit-dependencies
import * as Adapter from 'enzyme-adapter-react-16';
import { Button } from '@material-ui/core';

Enzyme.configure({ adapter: new Adapter() });
/**
 * check whether our component renders correctly
 */
it("renders without crashing", () => {
    const mockFn: any = jest.fn();
    const component = Enzyme.shallow(<RepositoryForm {...mockFn} />);
    expect(component).toBeDefined();
});

/**
 * check if handleSubmit function is called when button is pressed
 */
it('handleSubmit should fire when clicked', () => {
    const mockFn = jest.fn();
    const wrapper = Enzyme.shallow(<RepositoryForm onSubmit={mockFn} />);
    //Expect to find the button
    expect(wrapper.find(Button).length).toBe(1);
    const button = wrapper.find(Button);
    console.log('props', wrapper.find(Button).props().onClick); //Da togliere

    button.simulate('click');
    expect(mockFn.mock.calls.length).toEqual(1);
});

