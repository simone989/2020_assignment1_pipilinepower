module.exports = {
  preset: 'ts-jest',
  setupFilesAfterEnv: ['jest-enzyme'],
  testEnvironment: 'enzyme',
  testMatch: ['**/__tests__/**/*.+(ts|tsx|js)', '**/?(*.)+(spec|test).+(ts|tsx|js)'],
  setupFiles: ['raf/polyfill'],
};
