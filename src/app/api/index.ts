import axios from 'axios';

export const getLastReleaseTagFromRepository = (): Promise<string> => {
  const url = `https://gitlab.com/api/v4/projects/21592432/repository/tags`
  return axios.get(url).then((response: any) => response.data[0])
}