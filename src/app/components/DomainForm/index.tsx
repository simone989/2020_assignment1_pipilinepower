import * as React from 'react';
import { Button, Paper, Grid, Typography } from '@material-ui/core';
import { useStyles } from './style';

export interface Props {
  onSubmit: () => void;
}

export interface State {
  username: string;
  password: string;
}

export const RepositoryForm = ({ onSubmit }: Props): JSX.Element => {
  const style = useStyles();


  const handleSubmit = () => {
      onSubmit()
    };

  return (
    <Paper elevation={1}>
      <Grid spacing={2} container direction="column" alignItems="center" justify="center" className={style.gridContainer}>
        <Grid item>
          <Typography variant="h6">Get last release tag from GitLab '2020_assignment1_pipilinepower' repository</Typography>
        </Grid>

        <Grid item className={style.fullWidth}>
          <Button fullWidth variant="contained" color="primary" disabled={false} onClick={handleSubmit}>GET</Button>
        </Grid>
      </Grid>
    </Paper>
  );
};
