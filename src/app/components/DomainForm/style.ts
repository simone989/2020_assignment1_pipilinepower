import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles({
  gridContainer: {
    padding: 32
  },
  fullWidth: {
    width: '100%'
  }
});
