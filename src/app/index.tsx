import React from 'react';
import { Route, Switch, Redirect } from 'react-router';
import { Home } from 'app/pages/Home';
import { hot } from 'react-hot-loader';

export const App = hot(module)(() => (
  <Switch>
    <Route path="/home" component={Home}/>
    <Redirect from="/" to="home"/>
  </Switch>
));
