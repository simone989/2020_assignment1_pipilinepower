
import * as React from 'react'
import { RouteComponentProps } from 'react-router';
import { Grid, Typography, Snackbar, Card } from '@material-ui/core';
import { RepositoryForm } from '../../components';
import { getLastReleaseTagFromRepository} from '../../api';
import ReactJson from 'react-json-view'
import { useStyles } from './style';

export interface Props extends RouteComponentProps<void> {}

export const Home = ({ history, location }: Props) => {
  const style = useStyles();
  const [result, setResult] = React.useState<any|undefined>(undefined);
  const [error, setError] = React.useState(false);
  const [errorMessage, setErrorMessage] = React.useState('');


  const getConfigByAlias = async () => {
    const commitObject = await getLastReleaseTagFromRepository()
    if (!commitObject) {
      setError(true);
      setErrorMessage(commitObject.toString());
    } else {
      setError(false);
      setResult(commitObject);
    }
  }

  return (
    <Grid container direction="column" alignItems="center" justify="center" className={style.gridContainer}>
      <Card>
      <RepositoryForm onSubmit={getConfigByAlias} />
      <Grid spacing={2} container direction="column" alignItems="center" justify="center" className={style.resultGridContainer}>
        <Typography variant='h6'>LAST GIT TAG</Typography>
  {result ? <div id={'tag'}>{result.name}</div> : <Typography>Make a query first</Typography>}
        <Typography variant='h6'>RESULT</Typography>
        {result ? <ReactJson src={result}></ReactJson> : <Typography>Make a query first</Typography>}
      </Grid>
      <Snackbar autoHideDuration={2500} open={error} message={errorMessage} onClose={() => setError(false)} />
      </Card>
    </Grid>
  );
};
