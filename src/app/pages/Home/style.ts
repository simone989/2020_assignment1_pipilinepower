import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles({
  gridContainer: {
    minHeight: '100vh'
  },
  resultGridContainer: {
    padding: 32
  }
});
